---
layout: handbook-page-toc
title: "Marketing Enablement"
description: "Team focused to supporting and enabling the Marketing Department"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Team Charter
Marketing Enablement is dedicated to stakeholder training, governance, adoption, and process improvement. We support the marketing organization to enhance individual and team effectiveness, keep team members better informed of resources and updates; increasing their capability, knowledge, and skills to enable them to better reach prospects, customers, and the GitLab community. 

This page is under construction. Enablement is a new function of the Marketing Operations team and will evolve over time. If you have suggestions, ideas, or feedback, feel free to open an MR or reach out in #mktgops on Slack.  

## Meet the Team
| Person | Role |
| ------ | ------ |
| [Nikki Silverberg](https://gitlab.com/nikkiroth) | Staff Marketing Operations Manager |

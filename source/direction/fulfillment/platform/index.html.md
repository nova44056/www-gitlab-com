---
layout: markdown_page
title: "Product Direction - Fulfillment: Platform"
description: "The Fulfillment Platform team at GitLab focuses on providing the foundations to our billing system."
canonical_path: "/direction/fulfillment/platform/"
---
 
## On this page
{:.no_toc .hidden-md .hidden-lg}
 
- TOC
{:toc}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />
 
## Mission

> The Fulfillment Platform group provides foundational GitLab Order-To-Cash solutions. These solutions enable other fulfillment groups and contributors to ship with confidence, speed and efficiency, while ensuring that GitLab's fulfillment systems remain robust and performant.

## Overview

The Fulfillment Platform group owns, maintains and evolves the underlying infrastructure for GitlLab’s order-to-cash flow, including the responsibility for the [CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com/) application.

In order to enable other groups, contributors, and stakeholders, we are focusing on 3 key areas of the Fulfillment infrastructure:

| Key area                      | Principle                                                                                                                 | How                |
|-------------------------------|---------------------------------------------------------------------------------------------------------------------------|--------------------|
| System reliability            | Provide fulfillment infrastructure with best in class reliability and availability. | [CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com/) has a `99.95%` target [availability and must stay within allowed error budgets](https://dashboards.gitlab.net/d/stage-groups-detail-fulfillment_platform/stage-groups-fulfillment-platform-group-error-budget-detail?orgId=1). |
| Developer productivity        | Provide a great experience for every developer that contributes to fulfillment solutions.                   | Make code contribution easier by maintaining the underlying infrastructure, keeping it performant, observable and decreasing complexity through abstractions at the platform level. |
| Fulfillment process efficiency| Make fulfillment infrastructure and processes simple and easily digestible.               | Abstract integration complexity with Zuora and improve the underlying architecture of [CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com/) to better align with  the order-to-cash systems.  |

## Performance indicators

We use performance indicators (PI) to track our progress. Currently, our PI's proposal is covering all 3 key areas of our fulfillment platform work:
- System reliability (e.g. Availability)
- Developer productivity (e.g. Mean time to recovery)
- Fulfillment process efficiency (e.g. Data integrity)

We aim to have at least 1 leading PI per key area that helps us stay on track with our group's mission. The status of these PI's is reviewed monthly with product leadership. For the latest metrics, see our [GitLab Internal Handbook](https://internal-handbook.gitlab.io/handbook/company/performance-indicators/product/fulfillment-section/).

## Target audience

As our responsibilities mostly span backend-heavy and infrastructural work, our direct target audience is GitLab internally focused:

- Other fulfillment groups and team members
- Internal stakeholders (Customer Success, Sales, Billing)

We improve the experience of all of our customers (self-service, sales-assisted, and reseller customers) by improving fulfillment systems' process flows and subscription data accuracy.

## What's up now ( up to 12 months)

Within the next 12 months we want to strengthen the foundation of the fulfillment platform. Our main focus is to improve our order-to-cash systems and underlying data architecture, which will help us scale as GitLab continues to grow.

**Key projects for foundational strength are:**

- [Introduce and enable a CustomersDot Billing Account](https://gitlab.com/groups/gitlab-org/-/epics/8331)
- [Upgrade to Zuora Orders API](https://gitlab.com/groups/gitlab-org/-/epics/6438)
- [GitLab.com SSO as the only login option for CustomersDot to reduce data discrepency](https://gitlab.com/groups/gitlab-org/-/epics/8905)
- [Increase level maturity for CustomersDot](https://about.gitlab.com/handbook/engineering/infrastructure/service-maturity-model/#customersdot-detail)

For a full list of our upcoming and ongoing projects, check out our [Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?state=all&sort=end_date_asc&layout=QUARTERS&timeframe_range_type=THREE_YEARS&label_name%5B%5D=Fulfillment+Roadmap&label_name%5B%5D=group%3A%3Afulfillment+platform&progress=COUNT&show_progress=true&show_milestones=false&milestones_type=GROUP).

### InfraDev work 

As one of our key areas spans developer productivity we also work on performance, scalability, delivery and observability efforts with help from the [Infrastructure department](https://about.gitlab.com/handbook/engineering/infrastructure/).

To increase the [level maturity of CustomersDot](https://about.gitlab.com/handbook/engineering/infrastructure/service-maturity-model/#customersdot-detail) and continue improving our availability and observability we are working on:

| What                                                                                                                                 | Why                                                                             | When            |
|--------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------|-----------------|
| [Add a SLA (uptime/availability) dashboard](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible/-/issues/148)                | Add more visibility and tracking around CustomersDot SLA and availability       | In-progress     |
| [Improve current CustomersDot runbooks](https://gitlab.com/gitlab-com/runbooks/-/issues/99)                                          | Eliminate dependency on Fulfillment when troubleshooting CustomersDot           | 1-3 milestones  |
| (HA) [Migrate from postgreSQL to Cloud SQL](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible/-/issues/152)                | Less maintenance and better availability for the CustomersDot DB                | 1-3 milestones  |
| Improve redundancy between CustomersDot and Zuora                                                                                    | Adds a better caching layer to have no downtime due to frequent Zuora outages   | 1-3 milestones  |
| (HA) [Iteratively migrate CustomersDot to a HA environment](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/807)               | Move CustomersDot services/components to HA in order to improve reliability     | 3-12 milestones |
| [Add logs to Kibana](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible/-/issues/42)                                        | We have GCP logs but Kibana is more complete and part of the maturity levels    | 3-6 milestones  |
| [Implement auto-rollback on error](https://gitlab.com/gitlab-com/gl-infra/customersdot-ansible/-/issues/156)                         | Minimize disruption if an outage occurs after a deployment                      | 3-6 milestones  |
| (Business Metrics) [Alerting of failed jobs for critical SaaS metrics](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/808)    | Move away from noisy Sentry alerts to critical alerts for accurate SaaS billing | 3-6 milestones  |
| (Business Metrics) [Alerting of failed jobs for critical SM metrics](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/809)      | Move away from noisy Sentry alerts to critical alerts for accurate SM billing   | 3-6 milestones  |
| (Business Metrics) [Alerting over a threshold of payment failures](https://gitlab.com/gitlab-org/customers-gitlab-com/-/issues/4145) | Encounter payment problems as they occur                                        | 3-6 milestones  |

## Future oportunities

To be added
